# Equipe de desenvolvimento do cliente 2019

## Objetivos:
* Desenvolver a biblioteca cliente TCP/tetrinet em nodejs

## Grupo 6 - membros
* Marcos Vinícius de Souza Oliveira (Líder)
* Karlla Chaves (Developer)
* Luan Oliveira (Developer)
* Luis Eduardo Peixoto de Aquino (Developer)
* Cardeque Henrique (Developer)

# Equipe de desenvolvimento do cliente 2018

## Objetivos:
* Desenvolver a biblioteca cliente TCP/tetrinet em nodejs

## Grupo 4 - membros
* PEDRO HENRIQUE FERNANDES DOS REIS (líder)
* MATHEWS ARANTES PEREIRA
* RODRIGO DE OLIVEIRA GUIMARAES
* LUCAS DE OLIVEIRA NAVES
* TIAGO MOURA DE FARIA

## Tarefas da sprint 1 (25/03 a 31/03):
* Criar protótipo inicial
* Criar snippets de modelos de bibliotecas nodejs


###### Executar

* Para testar a aplicação, basta executar `node index.js`
