/*/
Envia ao servidor a informação que o jogador local pausou/despausou o jogo.
O servidor só responde se o jogador local que está enviando é o operador do canal.
pause <state> <playernum>
/*/
