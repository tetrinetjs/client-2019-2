//@ts-check
class Protocol {
    /**
     * 
     * @param {string} nickname 
     * @param {string} version 
     * @param {Array<number>} ip 
     * @param {boolean} tetrifast 
     */
    encodeLogin(nickname, version, ip, tetrifast) {
        let p = 54 * ip[0] + 41 * ip[1] + 29 * ip[2] + 17 * ip[3];
        let pattern = p.toString().split('');
        let mode = (tetrifast ? "tetrifaster " : "tetrisstart ").split('');
        let n = nickname.split('').concat(' ');
        let v = version.split('');

        let data = [mode, n, v];
        data = mode.concat(n).concat(v);

        let i;
        let current = 80;
        let encodedString = 80;
        let previous = 80;
        for (i = 0; i < data.length; i++) {
            var _char = data[i].charCodeAt(0);
            var sec = (pattern[i % pattern.length]);
            if (i > 0) {
                sec = sec.toString(16).charCodeAt(0);
            }
            current = ((previous + _char) % 255) ^ sec;
            encodedString += this._toHex(current);
            previous = current;
        }

        return this.encodeMessage(encodedString.toUpperCase() + String.fromCharCode(255));
    }

    /**
     * 
     * @param {string} message 
     */
    encodeMessage(message) {
        const buffer = Buffer.from(message, 'ascii');
        return Buffer.from(buffer, 'ascii');
    }

    /**
     * 
     * @param {string} c 
     */
    _toHex(c) {
        let h = c.toString(16);
        return h.length > 1 ? h : "0" + h
    }

    getPlayerNum(string) {
        if (string.length > 0) {
            const playernum = string.replace("playernum ", "");
            return playernum.replace("winlist", "");
        }
    }
    
    /**
     * 
     * @param {string} message 
     */
    decodeMessage(message) {
        message = message.slice(0, message.length - 1);
        const messageType = message.split(' ')[0];
        const decoderOfCurrentMessage = this._findDecoderFunctionToMessageType(messageType);
        return decoderOfCurrentMessage(message);
    }

    _findDecoderFunctionToMessageType(messageType) {
        const mapMessageTypeToDecoderFunction = {
            winlist: this._decodeWinlist,
            playernum: this._decodePlayerNum
        };
        return mapMessageTypeToDecoderFunction[messageType] || this._defaultDecoder
    }

    _decodeWinlist(message) {
        //return Buffer.from( [ 0x74, 0x65, 0x61, 0x6d, 0x20, 0x31, 0x20, 0xff ] );
        return message;
    }

    _decodePlayerNum(message) {
        // message = message.replace(/[^\x00-\x7E]/g, '\n');
        // let msgs = message.split("\n");
        // return msgs[0];
        return message;
    }

    _defaultDecoder(message) {
        // return message.split(' ')[1];
        return message
    }
}

module.exports.Protocol = Protocol;