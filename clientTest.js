const TetrinetClient = require('./src/TetrinetClient/TetrinetClient').TetrinetClient;
var readline = require('readline');

const client = new TetrinetClient();
var playerNum = ''

client.connect({username: process.argv[2].toString()});
console.log("Connected player: " + process.argv[2].toString());

client.onReceiveData((message) => {
  if (message.substr(0,9) == 'playernum'){
    playerNum = message.substr(10,1);
    //console.log(playerNum)
    return 0;
  }
  if(message.substr(0,5) == 'pline'){
    console.log("Player " + message.substr(6,1) +":" +message.substr(8,message.length));
    return 0;
  }
  console.log("Recebido do Server:" + message);
});

client.onError((err) => {
  console.log(err);
  console.log('teste reprovado');
}); 

setTimeout(showMenu, 1500);

var readline = require('readline');
var log = console.log;

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var recursiveAsyncReadLine = function () {
  rl.question('', function (answer) {
    if (answer == '/exit'){
      return rl.close();
     }
     else if (answer == '/start'){
      const comeca = ('startgame 1 '+ playerNum)
      client.startGame(comeca);
     }
     else if (answer == '/end'){
      const fim = ('endgame 0 '+ playerNum)
      client.endGame(fim);
     }
     else if (answer == '/logout'){
     	const sair = ('leavegame ' + playerNum)
     	client.logOff(sair);
     }
     else if (answer.substr(0,2) == '/m'){
       const message = answer.replace('/m ','');
       client.sendChatMessage(message,playerNum);
     }
     else if (answer == '/menu'){
      showMenu();
     }
     else if (answer == '/pause'){
      const pause = ('pause 1 '+ playerNum)
      client.pauseGame(pause);
     }
     else if (answer == '/unpause'){
      const unpause = ('pause 0 '+ playerNum)
      client.unPauseGame(unpause);
     }
    recursiveAsyncReadLine();
  });
};

recursiveAsyncReadLine();

function showMenu() {
  console.log("\n---MENU---\n/start - to start the game\n/m message - to send message to players\n/logout - to leave the game\n/end - to finish the game\n/menu - to show the menu\n/pause - to pause the game\n/unpause - to unpause the game");
}









